var createElement  = require("create-element");


var createAndReturnElement = function ( element, where, tagName, attributes) {
    element.insertAdjacentHTML(where, createElement( tagName, attributes || {}));
    switch( where ) {
        case "afterbegin":  return element.firstChild;
        case "beforeend":   return element.lastChild;
        case "afterend":    return element.nextSibling;
        case "beforebegin": return element.previousSibling;
    }
    throw new Error("Don't know which element to return. New node was inserted " + where + "?!");
};

module.exports = createAndReturnElement;
