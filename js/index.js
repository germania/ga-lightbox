var extend                 = require("extend"),
    setAttributes          = require("ga-setattributes"),
    delegate               = require("delegate"),
    disableScroll          = require("disable-scroll"),
    createElement          = require("create-element"),
    Emitter                = require("component-emitter"),
    isDom                  = require("is-dom"),
    isString               = require("is-string"),
    createAndReturnElement = require("create-return-element");



/**
 * @param element|string selector
 * @param JSON           options
 */
var Lightbox = function( element, options ) {
    this.settings = extend(true, {

        tagName             : "figure",
        attributes          : {
            class: "ga-lightbox"
        },

        adjustPosition      : function() { },
        disableScroll       : true,


        imageContainer : {
            tagName : "div",
            attributes: {
                class : "lightbox-image-container",
                "data-lightbox" : null,
                "style" : {
                    backgroundImage: "none"
                }
            }
        },

        image: {
            tagName: "img",
            attributes: {
                src: "",
                alt: "",
                title: ""
            }
        },

        toolbar: {
            tagName: "figcaption",
            attributes: {
                class           : "lightbox-toolbar",
                title           : "",
                "data-lightbox" : null,
                "style" : {
                    backgroundImage: ""
                }
            }
        },

        keyboardEventSource : document,


        hidden: {
          hidden: "hidden",
          "aria-hidden": "hidden"
        },

        visible: {
          hidden: null,
          "aria-hidden": false
        },



        keyboard: {
            close:    [27,32],
            // next:     [39,40],
            // previous: [37,38]
        },


        pointer: {
            close:     ["touchstart", "click"],
            //next:      ["touchstart", "click"],
            //previous:  ["touchstart", "click"]
        },

        buttons: {
            close:    "Close",
            //next:     {
            //    name: "Next",
            //    attributes: {}
            //}
            //previous: "Previous"
        },




    }, options || {});

    // Deal with string-like elements
    if (isString(element)) {
        this.settings.attributes = extend(true, this.settings.attributes, { id: element });
    }

    var elements = setupBoxElement( element, this.settings );
    this.element   = elements.element;
    this.container = elements.imageContainer;
    this.image     = elements.image;
    this.toolbar   = elements.toolbar;

    // Initially hide
    this.hide();


    setupPointerEvents(this, this.settings.pointer);

    // Make this usable for addEventHandler and removeEventHandler
    this._keyHandler = this.handleKeyEvent.bind(this);


    // Default event handlers
    this.on("close", function() {
        this.hide();
    });

    this.on("reveal", function() {
        if (typeof(this.settings.adjustPosition) === typeof(Function)) {
            this.settings.adjustPosition( this.element, this );
        }
    }.bind(this));

    // Finished
    this.emit("init", this);
};



// Make this an event emitter
Emitter(Lightbox.prototype);





Lightbox.prototype.reveal = function() {
    this.settings.disableScroll && disableScroll.on();
    this.settings.keyboard      && this.listenKeyboard( true );

    setAttributes( this.element, this.settings.visible );

    this.emit("reveal", this);
    return this;
};


Lightbox.prototype.hide = function() {
    this.settings.disableScroll && disableScroll.off();
    this.settings.keyboard      && this.listenKeyboard( false );

    setAttributes(this.element, this.settings.hidden );

    this.emit("hide", this);
    return this;
};




Lightbox.prototype.setImage = function( src, attributes) {

    // Change URL in image element
    setAttributes( this.image, extend(true, {},
        this.settings.image.attributes,
        this.settings.visible,
        attributes || {},
        {
            src: src
        }
    ));

    // Reset container if used with background image before
    setAttributes( this.container, this.settings.imageContainer.attributes);

    // Time for change event
    this.emit("change", this);
    return this;
};



Lightbox.prototype.setBackgroundImage = function( src, attributes) {

    // Set background-image to container
    setAttributes( this.container, extend(true, {},
        this.settings.imageContainer.attributes,
        attributes || {}, {
            "data-lightbox": "background",
            "style": {
                backgroundImage: "url(" + src + ")"
            }
        }
    ));

    // Reset image if used before
    setAttributes( this.image, extend(true, {},
        this.settings.image.attributes,
        this.settings.hidden));

    // Time for change event
    this.emit("change", this);

    return this;
};







Lightbox.prototype.handleKeyEvent = function( e ) {

    Object.keys(this.settings.keyboard || {}).forEach( function( name ) {
        if (this.settings.keyboard[name].constructor === Array) {

            if (this.settings.keyboard[name].some(function(code) {
                return e.keyCode === code;
            })) {
                //console.info( name );
                this.emit( name, this );
            }
        }
    }.bind(this));

};


Lightbox.prototype.listenKeyboard = function( listen_keyboard ) {
    //console.info("Listen to Keyboard", listen_keyboard);

    if (listen_keyboard) {
        this.settings.keyboardEventSource.addEventListener("keydown", this._keyHandler );
    }
    else {
        this.settings.keyboardEventSource.removeEventListener("keydown", this._keyHandler );
    }
    return this;
};




module.exports = Lightbox;




/**
 * Returns the lightbox element to work on
 */
// element.charAt(0) === "#")
var setupBoxElement = function( element, settings ) {

    // Accept only DOM elements or string
    if (!isDom( element ) && !isString(element)) {
        throw( new TypeError("Either string ID or DOM element expected") );
    }

    // Create lightbox element if no existant
    if (isString( element )) {
        element = document.getElementById( element ) || createAndReturnElement(document.body, "beforeend", settings.tagName , settings.attributes);
    }

    // Apply lightbox default attributes
    setAttributes( element, settings.attributes);

    // We will store important elements here
    var result = {
        element: element
    };

    // Create image container, image and toolbar
    result.imageContainer = createAndReturnElement(element, "afterbegin", settings.imageContainer.tagName, settings.imageContainer.attributes);
    result.image          = createAndReturnElement(result.imageContainer, "afterbegin", settings.image.tagName, settings.image.attributes);
    result.toolbar        = createAndReturnElement(element, "beforeend", settings.toolbar.tagName, settings.toolbar.attributes);

    // Fill in buttons
    Object.keys( settings.buttons ).forEach(function( n ) {

        var button = settings.buttons[n];

        // If item is string only...
        if (typeof button === "string" || button instanceof String) {
            button = extend(true, {
                name: button,
                attributes: {}
            });
        }

        // Create DOM attributes
        var button_attributes = extend(true, {}, button.attributes, {
            "class": button.class || button.attributes.class || {},
            "data-lightbox-event": n
        });

        // Create Button
        result.toolbar.insertAdjacentHTML("beforeend", createElement("button", button_attributes, button.name));
    });


    // Result should look like this
    // {
    //    element        : element,
    //    imageContainer : element,
    //    image          : element
    //    toolbar        : element
    // }
    return result;

};

var setupPointerEvents = function( emitter, pointer_events) {
    // Each pointer event:
    Object.keys( pointer_events ).forEach( function( emit_event ) {

        // Each event type: touchstart, click ...
        pointer_events[ emit_event ].forEach(function( event_type ) {
            delegate(document.body, "[data-lightbox-event=" + emit_event + "]", event_type, function( e ) {
                // console.log( emit_event );
                e.preventDefault();
                var event_type = e.delegateTarget.getAttribute("data-lightbox-event");
                emitter.emit( event_type );
            });
        });
    });
};



