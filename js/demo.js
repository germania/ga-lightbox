// This is not production code, for demo page only.

var Lightbox = require("index"),
    delegate = require("delegate");

document.addEventListener("DOMContentLoaded", function() {

    var lightbox = new Lightbox( "moin", {
        // options go here
        attributes: {
            class: "ga-lightbox ga-lightbox-theme"
        }
    });



    delegate(document.body, ".default [data-lightbox]", "click", function(e) {
        console.log( e.delegateTarget.getAttribute("[data-lightbox]") );
        lightbox.setImage(e.delegateTarget.getAttribute("data-lightbox"), {
                alt: e.delegateTarget.alt,
                title: e.delegateTarget.title
        }).reveal();

    }, false);

    delegate(document.body, ".background [data-lightbox]", "click", function(e) {
        console.log( e.delegateTarget.getAttribute("[data-lightbox]") );
        lightbox.setBackgroundImage(e.delegateTarget.getAttribute("data-lightbox")).reveal();

    }, false);


});
