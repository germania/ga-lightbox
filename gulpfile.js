// Grundlegendes
var gulp          = require ("gulp"),
    webpack       = require ("webpack"),
    webpackConfig = require("./webpack.config.js");

// Plugins einbauen
var jshint       = require ("gulp-jshint"),
    uglify       = require ("gulp-uglify"),
    concat       = require ("gulp-concat"),
    gutil        = require ("gulp-util"),
    less         = require ("gulp-less"),
    minifyCss    = require ("gulp-minify-css"),
    rename       = require ("gulp-rename"),
    autoprefixer = require ("gulp-autoprefixer"),
    sourcemaps   = require('gulp-sourcemaps');


var GLOB_LESS    = "./less/**/*.less",
    GLOB_JS      = "./js/**/*.js",

    DIST_CSS     = './dist',
    DIST_JS      = './dist';


// ================================
// Scripts
// ================================


gulp.task("lint", function() {
    return gulp.src( GLOB_JS )
        .pipe(jshint(".jshintrc"))
        .pipe(jshint.reporter("jshint-stylish"));
});


gulp.task('js', ["lint"], function () {
   return gulp.src( GLOB_JS )
      .pipe(sourcemaps.init())
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest( DIST_JS ));
});


//
// Development
//
var webpack_dev_config = Object.create( webpackConfig );

    // Set some dev-realted options
    webpack_dev_config.devtool = "source-map";
    webpack_dev_config.debug = true;
    //webpack_dev_config.debug = false;
    webpack_dev_config.cache = true;
    webpack_dev_config.bail = true;

var webpack_dev_compiler = webpack( webpack_dev_config );


gulp.task("webpack", ["lint"], function(callback) {

  return webpack_dev_compiler.run(function(err, stats) {
        if(err) throw new gutil.PluginError("webpack:dev", err);
        callback();
    });
});



// ================================
// Stylesheets
// ================================

gulp.task('styles', function () {
    return gulp.src( 'less/ga-lightbox.less' )
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie 9-11'],
            cascade: false
        }))
        .pipe(gulp.dest( DIST_CSS ))
        .pipe(minifyCss({compatibility: '*'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest( DIST_CSS ));
});


// ================================
// Common tasks
// ================================


//
// Rerun the task when a file changes
//
gulp.task('default', ["js", "webpack"]);



//
// Development builds:
// Rerun the task when a file changes
//
gulp.task('watch', function () {

    gulp.watch( GLOB_JS,  ["webpack"]);
    gulp.watch( GLOB_LESS , ["styles"]);

});

