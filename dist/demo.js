/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	// This is not production code, for demo page only.
	
	var Lightbox = __webpack_require__(1),
	    delegate = __webpack_require__(4);
	
	document.addEventListener("DOMContentLoaded", function() {
	
	    var lightbox = new Lightbox( "moin", {
	        // options go here
	        attributes: {
	            class: "ga-lightbox ga-lightbox-theme"
	        }
	    });
	
	
	
	    delegate(document.body, ".default [data-lightbox]", "click", function(e) {
	        console.log( e.delegateTarget.getAttribute("[data-lightbox]") );
	        lightbox.setImage(e.delegateTarget.getAttribute("data-lightbox"), {
	                alt: e.delegateTarget.alt,
	                title: e.delegateTarget.title
	        }).reveal();
	
	    }, false);
	
	    delegate(document.body, ".background [data-lightbox]", "click", function(e) {
	        console.log( e.delegateTarget.getAttribute("[data-lightbox]") );
	        lightbox.setBackgroundImage(e.delegateTarget.getAttribute("data-lightbox")).reveal();
	
	    }, false);
	
	
	});


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var extend                 = __webpack_require__(2),
	    setAttributes          = __webpack_require__(3),
	    delegate               = __webpack_require__(4),
	    disableScroll          = __webpack_require__(7),
	    createElement          = __webpack_require__(8),
	    Emitter                = __webpack_require__(9),
	    isDom                  = __webpack_require__(10),
	    isString               = __webpack_require__(11),
	    createAndReturnElement = __webpack_require__(12);
	
	
	
	/**
	 * @param element|string selector
	 * @param JSON           options
	 */
	var Lightbox = function( element, options ) {
	    this.settings = extend(true, {
	
	        tagName             : "figure",
	        attributes          : {
	            class: "ga-lightbox"
	        },
	
	        adjustPosition      : function() { },
	        disableScroll       : true,
	
	
	        imageContainer : {
	            tagName : "div",
	            attributes: {
	                class : "lightbox-image-container",
	                "data-lightbox" : null,
	                "style" : {
	                    backgroundImage: "none"
	                }
	            }
	        },
	
	        image: {
	            tagName: "img",
	            attributes: {
	                src: "",
	                alt: "",
	                title: ""
	            }
	        },
	
	        toolbar: {
	            tagName: "figcaption",
	            attributes: {
	                class           : "lightbox-toolbar",
	                title           : "",
	                "data-lightbox" : null,
	                "style" : {
	                    backgroundImage: ""
	                }
	            }
	        },
	
	        keyboardEventSource : document,
	
	
	        hidden: {
	          hidden: "hidden",
	          "aria-hidden": "hidden"
	        },
	
	        visible: {
	          hidden: null,
	          "aria-hidden": false
	        },
	
	
	
	        keyboard: {
	            close:    [27,32],
	            // next:     [39,40],
	            // previous: [37,38]
	        },
	
	
	        pointer: {
	            close:     ["touchstart", "click"],
	            //next:      ["touchstart", "click"],
	            //previous:  ["touchstart", "click"]
	        },
	
	        buttons: {
	            close:    "Close",
	            //next:     {
	            //    name: "Next",
	            //    attributes: {}
	            //}
	            //previous: "Previous"
	        },
	
	
	
	
	    }, options || {});
	
	    // Deal with string-like elements
	    if (isString(element)) {
	        this.settings.attributes = extend(true, this.settings.attributes, { id: element });
	    }
	
	    var elements = setupBoxElement( element, this.settings );
	    this.element   = elements.element;
	    this.container = elements.imageContainer;
	    this.image     = elements.image;
	    this.toolbar   = elements.toolbar;
	
	    // Initially hide
	    this.hide();
	
	
	    setupPointerEvents(this, this.settings.pointer);
	
	    // Make this usable for addEventHandler and removeEventHandler
	    this._keyHandler = this.handleKeyEvent.bind(this);
	
	
	    // Default event handlers
	    this.on("close", function() {
	        this.hide();
	    });
	
	    this.on("reveal", function() {
	        if (typeof(this.settings.adjustPosition) === typeof(Function)) {
	            this.settings.adjustPosition( this.element, this );
	        }
	    }.bind(this));
	
	    // Finished
	    this.emit("init", this);
	};
	
	
	
	// Make this an event emitter
	Emitter(Lightbox.prototype);
	
	
	
	
	
	Lightbox.prototype.reveal = function() {
	    this.settings.disableScroll && disableScroll.on();
	    this.settings.keyboard      && this.listenKeyboard( true );
	
	    setAttributes( this.element, this.settings.visible );
	
	    this.emit("reveal", this);
	    return this;
	};
	
	
	Lightbox.prototype.hide = function() {
	    this.settings.disableScroll && disableScroll.off();
	    this.settings.keyboard      && this.listenKeyboard( false );
	
	    setAttributes(this.element, this.settings.hidden );
	
	    this.emit("hide", this);
	    return this;
	};
	
	
	
	
	Lightbox.prototype.setImage = function( src, attributes) {
	
	    // Change URL in image element
	    setAttributes( this.image, extend(true, {},
	        this.settings.image.attributes,
	        this.settings.visible,
	        attributes || {},
	        {
	            src: src
	        }
	    ));
	
	    // Reset container if used with background image before
	    setAttributes( this.container, this.settings.imageContainer.attributes);
	
	    // Time for change event
	    this.emit("change", this);
	    return this;
	};
	
	
	
	Lightbox.prototype.setBackgroundImage = function( src, attributes) {
	
	    // Set background-image to container
	    setAttributes( this.container, extend(true, {},
	        this.settings.imageContainer.attributes,
	        attributes || {}, {
	            "data-lightbox": "background",
	            "style": {
	                backgroundImage: "url(" + src + ")"
	            }
	        }
	    ));
	
	    // Reset image if used before
	    setAttributes( this.image, extend(true, {},
	        this.settings.image.attributes,
	        this.settings.hidden));
	
	    // Time for change event
	    this.emit("change", this);
	
	    return this;
	};
	
	
	
	
	
	
	
	Lightbox.prototype.handleKeyEvent = function( e ) {
	
	    Object.keys(this.settings.keyboard || {}).forEach( function( name ) {
	        if (this.settings.keyboard[name].constructor === Array) {
	
	            if (this.settings.keyboard[name].some(function(code) {
	                return e.keyCode === code;
	            })) {
	                //console.info( name );
	                this.emit( name, this );
	            }
	        }
	    }.bind(this));
	
	};
	
	
	Lightbox.prototype.listenKeyboard = function( listen_keyboard ) {
	    //console.info("Listen to Keyboard", listen_keyboard);
	
	    if (listen_keyboard) {
	        this.settings.keyboardEventSource.addEventListener("keydown", this._keyHandler );
	    }
	    else {
	        this.settings.keyboardEventSource.removeEventListener("keydown", this._keyHandler );
	    }
	    return this;
	};
	
	
	
	
	module.exports = Lightbox;
	
	
	
	
	/**
	 * Returns the lightbox element to work on
	 */
	// element.charAt(0) === "#")
	var setupBoxElement = function( element, settings ) {
	
	    // Accept only DOM elements or string
	    if (!isDom( element ) && !isString(element)) {
	        throw( new TypeError("Either string ID or DOM element expected") );
	    }
	
	    // Create lightbox element if no existant
	    if (isString( element )) {
	        element = document.getElementById( element ) || createAndReturnElement(document.body, "beforeend", settings.tagName , settings.attributes);
	    }
	
	    // Apply lightbox default attributes
	    setAttributes( element, settings.attributes);
	
	    // We will store important elements here
	    var result = {
	        element: element
	    };
	
	    // Create image container, image and toolbar
	    result.imageContainer = createAndReturnElement(element, "afterbegin", settings.imageContainer.tagName, settings.imageContainer.attributes);
	    result.image          = createAndReturnElement(result.imageContainer, "afterbegin", settings.image.tagName, settings.image.attributes);
	    result.toolbar        = createAndReturnElement(element, "beforeend", settings.toolbar.tagName, settings.toolbar.attributes);
	
	    // Fill in buttons
	    Object.keys( settings.buttons ).forEach(function( n ) {
	
	        var button = settings.buttons[n];
	
	        // If item is string only...
	        if (typeof button === "string" || button instanceof String) {
	            button = extend(true, {
	                name: button,
	                attributes: {}
	            });
	        }
	
	        // Create DOM attributes
	        var button_attributes = extend(true, {}, button.attributes, {
	            "class": button.class || button.attributes.class || {},
	            "data-lightbox-event": n
	        });
	
	        // Create Button
	        result.toolbar.insertAdjacentHTML("beforeend", createElement("button", button_attributes, button.name));
	    });
	
	
	    // Result should look like this
	    // {
	    //    element        : element,
	    //    imageContainer : element,
	    //    image          : element
	    //    toolbar        : element
	    // }
	    return result;
	
	};
	
	var setupPointerEvents = function( emitter, pointer_events) {
	    // Each pointer event:
	    Object.keys( pointer_events ).forEach( function( emit_event ) {
	
	        // Each event type: touchstart, click ...
	        pointer_events[ emit_event ].forEach(function( event_type ) {
	            delegate(document.body, "[data-lightbox-event=" + emit_event + "]", event_type, function( e ) {
	                // console.log( emit_event );
	                e.preventDefault();
	                var event_type = e.delegateTarget.getAttribute("data-lightbox-event");
	                emitter.emit( event_type );
	            });
	        });
	    });
	};
	
	
	


/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';
	
	var hasOwn = Object.prototype.hasOwnProperty;
	var toStr = Object.prototype.toString;
	
	var isArray = function isArray(arr) {
		if (typeof Array.isArray === 'function') {
			return Array.isArray(arr);
		}
	
		return toStr.call(arr) === '[object Array]';
	};
	
	var isPlainObject = function isPlainObject(obj) {
		if (!obj || toStr.call(obj) !== '[object Object]') {
			return false;
		}
	
		var hasOwnConstructor = hasOwn.call(obj, 'constructor');
		var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
		// Not own constructor property must be Object
		if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
			return false;
		}
	
		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own.
		var key;
		for (key in obj) {/**/}
	
		return typeof key === 'undefined' || hasOwn.call(obj, key);
	};
	
	module.exports = function extend() {
		var options, name, src, copy, copyIsArray, clone,
			target = arguments[0],
			i = 1,
			length = arguments.length,
			deep = false;
	
		// Handle a deep copy situation
		if (typeof target === 'boolean') {
			deep = target;
			target = arguments[1] || {};
			// skip the boolean and the target
			i = 2;
		} else if ((typeof target !== 'object' && typeof target !== 'function') || target == null) {
			target = {};
		}
	
		for (; i < length; ++i) {
			options = arguments[i];
			// Only deal with non-null/undefined values
			if (options != null) {
				// Extend the base object
				for (name in options) {
					src = target[name];
					copy = options[name];
	
					// Prevent never-ending loop
					if (target !== copy) {
						// Recurse if we're merging plain objects or arrays
						if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
							if (copyIsArray) {
								copyIsArray = false;
								clone = src && isArray(src) ? src : [];
							} else {
								clone = src && isPlainObject(src) ? src : {};
							}
	
							// Never move original objects, clone them
							target[name] = extend(deep, clone, copy);
	
						// Don't bring in undefined values
						} else if (typeof copy !== 'undefined') {
							target[name] = copy;
						}
					}
				}
			}
		}
	
		// Return the modified object
		return target;
	};
	


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!function(t,e){ true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (e), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):"object"==typeof exports?module.exports=e():t.setAttributes=e()}(this,function(){function t(t,e){Object.getOwnPropertyNames(e).forEach(function(n){"styles"!==n&&"style"!==n||"object"!=typeof e[n]?"html"===n?t.innerHTML=e[n]:null===e[n]?t.removeAttribute(n):n in t?t[n]=e[n]:t.setAttribute(n,e[n]):Object.getOwnPropertyNames(e[n]).forEach(function(o){t.style[o]=e[n][o]})})}return t});
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbInJvb3QiLCJmYWN0b3J5IiwiZGVmaW5lIiwiYW1kIiwiZXhwb3J0cyIsIm1vZHVsZSIsInNldEF0dHJpYnV0ZXMiLCJ0aGlzIiwiZWxlbWVudCIsImF0dHJpYnV0ZXMiLCJPYmplY3QiLCJnZXRPd25Qcm9wZXJ0eU5hbWVzIiwiZm9yRWFjaCIsIm5hbWUiLCJpbm5lckhUTUwiLCJyZW1vdmVBdHRyaWJ1dGUiLCJzZXRBdHRyaWJ1dGUiLCJwcm9wIiwic3R5bGUiXSwibWFwcGluZ3MiOiJDQUFDLFNBQVVBLEVBQU1DLEdBQ1Msa0JBQVhDLFNBQXlCQSxPQUFPQyxJQUV2Q0QsVUFBV0QsR0FDZSxnQkFBWkcsU0FJZEMsT0FBT0QsUUFBVUgsSUFHakJELEVBQUtNLGNBQWdCTCxLQUUzQk0sS0FBTSxXQUVKLFFBQVNELEdBQWdCRSxFQUFTQyxHQUU5QkMsT0FBT0Msb0JBQXFCRixHQUFhRyxRQUFRLFNBQVVDLEdBRXpDLFdBQVRBLEdBQThCLFVBQVRBLEdBQWlELGdCQUFyQkosR0FBV0ksR0FLL0MsU0FBVEEsRUFDTEwsRUFBUU0sVUFBWUwsRUFBV0ksR0FFTCxPQUFyQkosRUFBV0ksR0FDaEJMLEVBQVFPLGdCQUFpQkYsR0FFcEJBLElBQVFMLEdBQ2JBLEVBQVNLLEdBQVNKLEVBQVdJLEdBRzdCTCxFQUFRUSxhQUFjSCxFQUFPSixFQUFXSSxJQWR4Q0gsT0FBT0Msb0JBQXFCRixFQUFXSSxJQUFRRCxRQUFRLFNBQVVLLEdBQzdEVCxFQUFRVSxNQUFNRCxHQUFRUixFQUFXSSxHQUFNSSxPQWtCdkQsTUFBT1giLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKHJvb3QsIGZhY3RvcnkpIHtcbiAgICBpZiAodHlwZW9mIGRlZmluZSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpIHtcbiAgICAgICAgLy8gQU1ELiBSZWdpc3RlciBhcyBhbiBhbm9ueW1vdXMgbW9kdWxlLlxuICAgICAgICBkZWZpbmUoW10sIGZhY3RvcnkpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgPT09IFwib2JqZWN0XCIpIHtcbiAgICAgICAgLy8gTm9kZS4gRG9lcyBub3Qgd29yayB3aXRoIHN0cmljdCBDb21tb25KUywgYnV0XG4gICAgICAgIC8vIG9ubHkgQ29tbW9uSlMtbGlrZSBlbnZpcm9ubWVudHMgdGhhdCBzdXBwb3J0IG1vZHVsZS5leHBvcnRzLFxuICAgICAgICAvLyBsaWtlIE5vZGUuXG4gICAgICAgIG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSggKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyBCcm93c2VyIGdsb2JhbHMgKHJvb3QgaXMgd2luZG93KVxuICAgICAgICByb290LnNldEF0dHJpYnV0ZXMgPSBmYWN0b3J5KCk7XG4gICAgfVxufSh0aGlzLCBmdW5jdGlvbiAoKSB7XG5cbiAgICBmdW5jdGlvbiBzZXRBdHRyaWJ1dGVzICggZWxlbWVudCwgYXR0cmlidXRlcyApIHtcblxuICAgICAgICBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyggYXR0cmlidXRlcyApLmZvckVhY2goZnVuY3Rpb24oIG5hbWUgKSB7XG5cbiAgICAgICAgICAgIGlmICgobmFtZSA9PT0gXCJzdHlsZXNcIiB8fCBuYW1lID09PSBcInN0eWxlXCIpICYmIHR5cGVvZiBhdHRyaWJ1dGVzW25hbWVdID09PSBcIm9iamVjdFwiKSB7XG4gICAgICAgICAgICAgICAgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoIGF0dHJpYnV0ZXNbbmFtZV0gKS5mb3JFYWNoKGZ1bmN0aW9uKCBwcm9wICkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnN0eWxlW3Byb3BdID0gYXR0cmlidXRlc1tuYW1lXVtwcm9wXTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKG5hbWUgPT09IFwiaHRtbFwiKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5pbm5lckhUTUwgPSBhdHRyaWJ1dGVzW25hbWVdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoYXR0cmlidXRlc1tuYW1lXSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCBuYW1lICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChuYW1lIGluIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50WyBuYW1lIF0gPSBhdHRyaWJ1dGVzW25hbWVdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoIG5hbWUgLCBhdHRyaWJ1dGVzW25hbWVdICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHJldHVybiBzZXRBdHRyaWJ1dGVzO1xufSkpO1xuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var closest = __webpack_require__(5);
	
	/**
	 * Delegates event to a selector.
	 *
	 * @param {Element} element
	 * @param {String} selector
	 * @param {String} type
	 * @param {Function} callback
	 * @param {Boolean} useCapture
	 * @return {Object}
	 */
	function delegate(element, selector, type, callback, useCapture) {
	    var listenerFn = listener.apply(this, arguments);
	
	    element.addEventListener(type, listenerFn, useCapture);
	
	    return {
	        destroy: function() {
	            element.removeEventListener(type, listenerFn, useCapture);
	        }
	    }
	}
	
	/**
	 * Finds closest match and invokes callback.
	 *
	 * @param {Element} element
	 * @param {String} selector
	 * @param {String} type
	 * @param {Function} callback
	 * @return {Function}
	 */
	function listener(element, selector, type, callback) {
	    return function(e) {
	        e.delegateTarget = closest(e.target, selector, true);
	
	        if (e.delegateTarget) {
	            callback.call(element, e);
	        }
	    }
	}
	
	module.exports = delegate;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var matches = __webpack_require__(6)
	
	module.exports = function (element, selector, checkYoSelf) {
	  var parent = checkYoSelf ? element : element.parentNode
	
	  while (parent && parent !== document) {
	    if (matches(parent, selector)) return parent;
	    parent = parent.parentNode
	  }
	}


/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';
	
	var proto = Element.prototype;
	var vendor = proto.matches
	  || proto.matchesSelector
	  || proto.webkitMatchesSelector
	  || proto.mozMatchesSelector
	  || proto.msMatchesSelector
	  || proto.oMatchesSelector;
	
	module.exports = match;
	
	/**
	 * Match `el` to `selector`.
	 *
	 * @param {Element} el
	 * @param {String} selector
	 * @return {Boolean}
	 * @api public
	 */
	
	function match(el, selector) {
	  if (vendor) return vendor.call(el, selector);
	  var nodes = el.parentNode.querySelectorAll(selector);
	  for (var i = 0; i < nodes.length; i++) {
	    if (nodes[i] == el) return true;
	  }
	  return false;
	}

/***/ },
/* 7 */
/***/ function(module, exports) {

	var disableScroll = {
	    // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
	    // left: 37, up: 38, right: 39, down: 40
	    options: {
	        disableWheel: true,
	        disableScrollbar: true,
	        disableKeys: true,
	        scrollEventKeys: [32, 33, 34, 35, 36, 37, 38, 39, 40]
	    },
	    element: document.body,
	    lockToScrollPos: [0, 0],
	
	    /**
	     * Disable Page Scroll
	     * @external Node
	     *
	     * @param {external:Node} [element] - DOM Element, usually document.body
	     * @param {object} [options] - Change the initial options
	     */
	    on: function (element, options) {
	        this.element = element || document.body;
	        this.options = this._extend(this.options, options);
	
	        if (this.options.disableWheel) {
	            document.addEventListener('mousewheel', this._handleWheel);
	            document.addEventListener('DOMMouseScroll', this._handleWheel);
	            document.addEventListener('touchmove', this._handleWheel);
	        }
	
	        if (this.options.disableScrollbar) {
	            this.lockToScrollPos = [
	                this.element.scrollLeft,
	                this.element.scrollTop
	            ];
	            this._disableScrollbarFn = this._handleScrollbar.bind(this);
	            document.addEventListener('scroll', this._disableScrollbarFn);
	        }
	
	        if (this.options.disableKeys) {
	            this._disableKeysFn = this._handleKeydown.bind(this);
	            document.addEventListener('keydown', this._disableKeysFn);
	        }
	    },
	
	    /**
	     * Re-enable page scrolls
	     */
	    off: function () {
	        document.removeEventListener('mousewheel', this._handleWheel);
	        document.removeEventListener('DOMMouseScroll', this._handleWheel);
	        document.removeEventListener('touchmove', this._handleWheel);
	        document.removeEventListener('scroll', this._disableScrollbarFn);
	        document.removeEventListener('keydown', this._disableKeysFn);
	    },
	
	    _handleWheel: function (e) {
	        e.preventDefault();
	    },
	
	    _handleScrollbar: function () {
	        window.scrollTo(this.lockToScrollPos[0], this.lockToScrollPos[1]);
	    },
	
	    _handleKeydown: function (event) {
	        for (var i = 0; i < this.options.scrollEventKeys.length; i++) {
	            if (event.keyCode === this.options.scrollEventKeys[i]) {
	                event.preventDefault();
	                return false;
	            }
	        }
	    },
	
	    _extend: function (obj) {
	        Object.keys(Array.prototype.slice(arguments, 1)).forEach(function (source) {
	            for (var prop in source) {
	                if (source.hasOwnProperty(prop)) {
	                    obj[prop] = source[prop];
	                }
	            }
	        });
	        return obj;
	    }
	};
	
	module.exports = disableScroll;


/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = createElement
	createElement.openingTag = openingTag
	createElement.closingTag = closingTag
	createElement.attributes = createAttributes
	var selfClosingTags = createElement.selfClosingTags = {
	  meta: true,
	  img: true,
	  link: true,
	  input: true,
	  source: true,
	  area: true,
	  base: true,
	  col: true,
	  br: true,
	  hr: true
	}
	
	/*
	
	  tagName [string]
	  attributes [object] (optional)
	  block [string || function] (optional)
	
	*/
	function createElement(tagName, attributes, block) {
	  if (~['function', 'string'].indexOf(typeof attributes)) {
	    block = attributes
	    attributes = null
	  }
	
	  return openingTag(tagName, attributes) +
	    (block ? (typeof block === 'function' ? block.call(this, '') : block) : '') +
	    (block || !selfClosingTags[tagName] ? closingTag(tagName) : '')
	}
	
	function openingTag(tagName, attributes) {
	  return '<' + tagName +
	    (attributes ? createAttributes(attributes) : '') +
	    '>'
	}
	
	function closingTag(tagName) {
	  return '</' + tagName + '>'
	}
	
	/*
	
	  attributes [object]
	
	    [string] : [string || array of strings || object of booleans || boolean]
	
	  result will have a leading space
	
	*/
	function createAttributes(attributes) {
	  var buf = ''
	
	  Object.keys(attributes).forEach(function (attribute) {
	    var value = attributes[attribute]
	    if (!value && value !== '') return;
	
	    value = Array.isArray(value) ? validValues(value)
	      : Object(value) === value ? validKeys(value)
	      : value
	    if (!value && value !== '') return;
	
	    buf += ' ' + attribute
	    if (value !== true) buf += '="' + value + '"';
	  })
	
	  return buf
	}
	
	function validValues(array) {
	  return array.filter(Boolean).join(' ')
	}
	
	function validKeys(object) {
	  return Object.keys(object).filter(function (key) {
	    return object[key]
	  }).join(' ')
	}


/***/ },
/* 9 */
/***/ function(module, exports) {

	
	/**
	 * Expose `Emitter`.
	 */
	
	module.exports = Emitter;
	
	/**
	 * Initialize a new `Emitter`.
	 *
	 * @api public
	 */
	
	function Emitter(obj) {
	  if (obj) return mixin(obj);
	};
	
	/**
	 * Mixin the emitter properties.
	 *
	 * @param {Object} obj
	 * @return {Object}
	 * @api private
	 */
	
	function mixin(obj) {
	  for (var key in Emitter.prototype) {
	    obj[key] = Emitter.prototype[key];
	  }
	  return obj;
	}
	
	/**
	 * Listen on the given `event` with `fn`.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */
	
	Emitter.prototype.on =
	Emitter.prototype.addEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
	    .push(fn);
	  return this;
	};
	
	/**
	 * Adds an `event` listener that will be invoked a single
	 * time then automatically removed.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */
	
	Emitter.prototype.once = function(event, fn){
	  function on() {
	    this.off(event, on);
	    fn.apply(this, arguments);
	  }
	
	  on.fn = fn;
	  this.on(event, on);
	  return this;
	};
	
	/**
	 * Remove the given callback for `event` or all
	 * registered callbacks.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */
	
	Emitter.prototype.off =
	Emitter.prototype.removeListener =
	Emitter.prototype.removeAllListeners =
	Emitter.prototype.removeEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	
	  // all
	  if (0 == arguments.length) {
	    this._callbacks = {};
	    return this;
	  }
	
	  // specific event
	  var callbacks = this._callbacks['$' + event];
	  if (!callbacks) return this;
	
	  // remove all handlers
	  if (1 == arguments.length) {
	    delete this._callbacks['$' + event];
	    return this;
	  }
	
	  // remove specific handler
	  var cb;
	  for (var i = 0; i < callbacks.length; i++) {
	    cb = callbacks[i];
	    if (cb === fn || cb.fn === fn) {
	      callbacks.splice(i, 1);
	      break;
	    }
	  }
	  return this;
	};
	
	/**
	 * Emit `event` with the given args.
	 *
	 * @param {String} event
	 * @param {Mixed} ...
	 * @return {Emitter}
	 */
	
	Emitter.prototype.emit = function(event){
	  this._callbacks = this._callbacks || {};
	  var args = [].slice.call(arguments, 1)
	    , callbacks = this._callbacks['$' + event];
	
	  if (callbacks) {
	    callbacks = callbacks.slice(0);
	    for (var i = 0, len = callbacks.length; i < len; ++i) {
	      callbacks[i].apply(this, args);
	    }
	  }
	
	  return this;
	};
	
	/**
	 * Return array of callbacks for `event`.
	 *
	 * @param {String} event
	 * @return {Array}
	 * @api public
	 */
	
	Emitter.prototype.listeners = function(event){
	  this._callbacks = this._callbacks || {};
	  return this._callbacks['$' + event] || [];
	};
	
	/**
	 * Check if this emitter has `event` handlers.
	 *
	 * @param {String} event
	 * @return {Boolean}
	 * @api public
	 */
	
	Emitter.prototype.hasListeners = function(event){
	  return !! this.listeners(event).length;
	};


/***/ },
/* 10 */
/***/ function(module, exports) {

	/*global window*/
	
	/**
	 * Check if object is dom node.
	 *
	 * @param {Object} val
	 * @return {Boolean}
	 * @api public
	 */
	
	module.exports = function isNode(val){
	  if (!val || typeof val !== 'object') return false;
	  if (window && 'object' == typeof window.Node) return val instanceof window.Node;
	  return 'number' == typeof val.nodeType && 'string' == typeof val.nodeName;
	}


/***/ },
/* 11 */
/***/ function(module, exports) {

	'use strict';
	
	var strValue = String.prototype.valueOf;
	var tryStringObject = function tryStringObject(value) {
		try {
			strValue.call(value);
			return true;
		} catch (e) {
			return false;
		}
	};
	var toStr = Object.prototype.toString;
	var strClass = '[object String]';
	var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';
	
	module.exports = function isString(value) {
		if (typeof value === 'string') { return true; }
		if (typeof value !== 'object') { return false; }
		return hasToStringTag ? tryStringObject(value) : toStr.call(value) === strClass;
	};


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var createElement  = __webpack_require__(8);
	
	
	var createAndReturnElement = function ( element, where, tagName, attributes) {
	    element.insertAdjacentHTML(where, createElement( tagName, attributes || {}));
	    switch( where ) {
	        case "afterbegin":  return element.firstChild;
	        case "beforeend":   return element.lastChild;
	        case "afterend":    return element.nextSibling;
	        case "beforebegin": return element.previousSibling;
	    }
	    throw new Error("Don't know which element to return. New node was inserted " + where + "?!");
	};
	
	module.exports = createAndReturnElement;


/***/ }
/******/ ]);
//# sourceMappingURL=demo.js.map