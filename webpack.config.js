var webpack = require("webpack");

var path = require('path');
var projectRoot = process.env.PWD; // Absolute path to the project root

module.exports = {
    context: "./js",

    entry: {
        "demo":  "demo"
    },

    output: {
        path: "./dist",
        filename: "[name].js" // Template based on keys in entry above
    },

    resolve: {

        extensions: [".js", "" ],

        modulesDirectories: [
          path.join(projectRoot, 'node_modules'),
          '.'
        ]
    }



};
