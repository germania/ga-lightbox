#ga-lightbox

**Another lightbox**

- Prepares your website to display large photos in a lightbox manner
- Two image display methods: via *img* element or *background-image*
- Both methods may be used at any time
- Custom element attributes supported
- Keyboard events supported
- Pointer events supported
- Custom toolbar buttons supported.
- Uses *hidden* and *aria-hidden* attributes by default

**What is it not?** This package does not convert a list of images into a magic slider-thumbnail-whatever-gallery-widget. 
Triggering a photo to be “lightboxed” is up to you.

#Installation witm NPM

```bash
$ npm install --save ga-lightbox
```

#Setup

###The CSS way

- Pre-built `dist/ga-lightbox.css`
- Minified `dist/ga-lightbox.min.css`

```html
<link rel="stylesheet" href="dist/ga-lightbox.css">
```

###The LESS way

- Use `less/ga-lightbox.less` with documentation as a boilerplate.
- Look for variables you may override in your project main file.

```css
@import "less/ga-lightbox.less"
```




#Instantiation

Just pass a DOM element or ID string to the Lightbox constructor. The lightbox element will be created if not existing.
Pass an optional options literal to the constrcutor. It will be deep-copied into the plugin defaults. 

```javascript
// Require
var Lightbox = require('ga-lightbox');

// Use existing element
var elem = document.getElementById("my-lightbox");
var lightbox = new Lightbox( elem, {} );

// Use string ID
var lightbox = new Lightbox( "#my-lightbox", {
	// options go here
});

```

After instantiation, lightbox HTML should look like this:

```html
<!-- The lightbox element and end of body -->
<figure class="ga-lightbox" hidden="hidden" aria-hidden="hidden" id="my-lightbox"></figure>
</body>
```


###BC Compat: Button style

As of version 2, toolbar buttons are unstyled by default. To have buttons styled as in previous package versions, just add *ga-lightbox-theme* to the lightbox' class attribute, like so:

```js
var lightbox = new Lightbox( "#my-lightbox", {
    attributes: {
        class: "ga-lightbox ga-lightbox-theme"
    }
});
```

##Changing images


```javascript
var src = "/images/foo.jpg",	
    attributes = {
        alt:   "value",
        title: "value"
    };
	

// Set image URL, optionally pass attributes:
lightbox.setImage( src);
lightbox.setImage( src, attributes);

lightbox.setBackgroundImage( src );
lightbox.setBackgroundImage( src, attributes );
```

##Showing and hiding

```javascript
// Show lightbox:
lightbox.reveal();

// Use chaining:
lightbox.setImage( src, attributes ).reveal();

// Hide lightbox:
lightbox.hide();
```





##Method API

###setImage( url [, attributes] )

Sets the image URL to display in lightbox. Applies any given attribute to the `img` element, see Component's [create-element](https://github.com/component/create-element) documentation on how to set attribute values. Example:

```javascript
lightbox.setImage("/foo.png", {
	alt:   "Lovely image",
	title: "Description of ...",
	class: {
    	yes: true,
	    no: false,
    	'photo': true
  	}
});
```

…will result in

```html
<img class="yes photo"  src="/foo.png"  alt="Lovely image" title="Description of ..." aria-hidden="false">
```



###setBackgroundImage( url [, attributes] )

Sets the image container's background-image URL to display as lightbox image. Like *setImage*, applies any given attributes to the image container.

```javascript
setBackgroundImage("/foo.png");
setBackgroundImage("/foo.png", { ... });
```

…will result in: 

```html
<div class="lightbox-image-container" data-lightbox="background" style="background-image: url('/foo.png');">
	<img src="" alt="" title="" aria-hidden="hidden" hidden>
</div>
```



###reveal()
Sets the hiding attributes to their “visible” values. Also disables scrolling and adjusts viewport position, if chosen in configuration options. After revealing, the lightbox HTML should look like this:

```html
<div id="my-lightbox" aria-hidden="false">
	<img alt="" title="" src="/images/foo.jpg">
</div>
```

###hide()
Adds the hiding attributes to their “hidden“ values. After that, the lightbox HTML should look like this:

```html
<div id="my-lightbox" aria-hidden="hidden" hidden="hidden">
	<img alt="" title="" src="/images/foo.jpg">
</div>
```



#Options

###adjustPosition

Callback for adjusting Lightbox's position in the viewport, when lightbox should be positioned `absolute`. You will likely not need this since `position: fixed` [support is fine](http://caniuse.com/#feat=css-fixed) as of Android 3 and iOS 8.

```
type: callback
default: false   
```

###attributes
The attributes the lightbox element gets. Add your own class attribute value to apply custom styles. To give tollbar buttons a basic style, add *ga-lightbox-theme* to the class.

```
type: Object literal
default: {
	class: "ga-lightbox"
}
```




###buttons
Configures the toolbar buttons. Each button will get a *data-lightbox-event* attribute with the option key as value and and the option value as text. See **pointer option** on event listening.

```
type: Object literal
default:  {
    close: "Close"
}
```

#####Advanced example
```
type: Object literal or string
default:  {
    close: "Close",
    next: {
        name: "Next",
        attributes: {
            class: "jump-next-button",
            title: "Next image, please"
        }
    }        
}
```



Given the default option, the buttons will look like this:
 
```html
<button data-lightbox-event="close">Close</button>
```




###disableScroll
Disables scrolling when Lightbox is revealed. Pass `false` to disable disabling. This functionality uses Gil Barbara's [disable-scroll package](https://github.com/gilbarbara/disable-scroll).

```
type: boolean
default: true
```

###hidden
When hiding elements (i.e. image element or the lightbox itself), these DOM attributes are set.

```
type: Object literal
default: {
    hidden: "hidden",
    "aria-hidden": "hidden"
}
```

###image
Default settings for the image element inside the lightbox image container.

```
type: Object literal
default: {
    tagName: "img",
    attributes: {
        src: "",
        alt: "",
        title: ""
    }
}
```


###imageContainer
Default settings for the image container inside the lightbox element.

```
type: Object literal
default: {
    tagName : "div",
    attributes: {
        class : "lightbox-image-container"
    }
}
```

###keyboard

Maps keyboard event codes to custom events that will be emitted. In this example, pressing Escape (27) and Space bar (32) fire "close" event. Per default, on *close* event the lightbox will be hidden. Disable keyboard control with empty literal or null.

```
type: Object literal
default: {
    close:    [27,32]
}
```

###keyboardEventSource

Experimental: When you have a gallery inside an iframe, and you click in it, Lightbox can listen to keyboard events from inside the iframe.

```
type: element
default: document
```

**Example:**

```javascript
var iframe    = document.querySelector( "#myframe" ),
	lightbox  = document.querySelector( "#box" );

var lightbox = new Lightbox( lightbox, {
	keyboardEventSource: iframe.contentWindow
};
```

###pointer
Configures on which events an event-emitting button fires. See **buttons option** on where to fire events.

```
type: Object literal
toolbar: {
    foo:       ["touchstart", "click"],
    close:     ["touchstart", "click"]
}
```

**Example:** If you have buttons with a *data-lightbox-event* attribute with an *event name* , these events will be fired on *touchstart* as well and *click*.

```html
<button data-lightbox-event="foo">Foo</button>
<button data-lightbox-event="close">Close</button>

<script>
lightbox.on("foo", function( lb ) {
	alert("Foo");
});
</script>
```




###tagName
The HTML tag to use for the lightbox.

```
type: string
default: "figure"
```

###toolbar
Default settings for the lightbox toolbar.

```
type: Object literal
toolbar: {
    tagName: "figcaption",
    attributes: {
        class           : "lightbox-toolbar",
        title           : "",
        "data-lightbox" : null,
        "style" : {
            backgroundImage: ""
        }
    }
}
```


###visible
When revealing elements (i.e. image element or the lightbox itself), these DOM attributes are set.

```
type: Object literal
default: {
    hidden: null,
    "aria-hidden": false
}
```

#Event API

On typical use cases, these events are fired:

```javascript
lightbox.on("init", function( lb ) {
	console.info("Lightbox constructed");
});

lightbox.on("change", function( lb ) {
	console.info("Lightbox image has changed");
});

lightbox.on("hide", function( lb ) {
	console.log("Lightbox now hidden");
});

lightbox.on("reveal", function( lb ) {
	console.log("Lightbox now visible");
});
```

Depending on keyboard or buttons configuration, these events are emitted as well. 
By default, only *close* event is supported:

```javascript
lightbox.on("close", function( lb ) {
	console.log("Lightbox closed");
});
```

#LESS/CSS Styling 

Inside `less/ga-lightbox.less` you will find these LESS variables with their respective default values. 
You may override them in your project LESS main file.

```less
// Global variables
@ga-lightbox-image-contain-breakpoint:               1440px;
@ga-lightbox-toolbar-height:                            8vh;
@ga-lightbox-toolbar-min-height:                       60px;
@ga-lightbox-background:                   rgba(0,0,0,0.85);

// Used in theme mode (class attribute contains "ga-lightbox-theme")
@ga-lightbox-toolbar-button-padding:            0.5rem 1rem;
@ga-lightbox-toolbar-button-border-radius:              1px;
@ga-lightbox-toolbar-button-text-color:          ButtonText;
@ga-lightbox-toolbar-button-bg-default-color:       #fcfcfc;
@ga-lightbox-toolbar-button-bg-hover-color:         #e9e9e9;
```

###About @ga-lightbox-image-contain-breakpoint

This variable defines the breakpoint where the background-image does not cover the lightbox any longer
but rather fully fits in it. The corresponding CSS reads like this:

```css
.ga-lightbox[data-lightbox="background"] {
    background-size: cover;
    @media (min-width: @ga-lightbox-image-contain-breakpoint) { 
      background-size: contain;  
    }
 }
```
 





#Roadmap

- This repo will move to GitHub.

#Develop with Gulp

Use [Git Flow](https://github.com/nvie/gitflow), always work in `develop` branch.

- Install development dependencies: `npm install`
- Run `gulp watch`
- Work in `js/` and `less/`

Builds are now in `dist/`



#Thanks
Cudos • honor • glory • appreciation go to:

- Component's [create-element](https://github.com/component/create-element) and [component-emitter ](https://github.com/component/emitter)package
- Gil Barbara's [disable-scroll package](https://github.com/gilbarbara/disable-scroll) package
- Forbes Lindesay's [closest](https://github.com/ForbesLindesay/closest) and [matches-selector](https://www.npmjs.com/package/matches-selector) packages
- Zeno Rocha's [delegate](https://github.com/zenorocha/delegate) package
- Yoshua Wuyts' [is-dom](https://www.npmjs.com/package/is-dom) pakage
- Jordan Harband's [is-string](https://www.npmjs.com/package/is-string) and [extend](https://www.npmjs.com/package/extend) packages

And, to make the list of requirements complete: [ga-setattributes](https://www.npmjs.com/package/ga-setattributes) package from the authors' own stuff.
